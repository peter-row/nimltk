[Package]
name          = "nimltk"
version       = "0.1.0"
author        = "Peter Row"
description   = "An attempt to port parts of NTLTK (nltk.org)"
license       = "Apache 2.0"

srcDir = "nimltk"
bin = "nimltk"
