import nltk,random
wn = nltk.wordnet.wordnet
all_synsets = list(wn.all_synsets())
all_nouns = [s for s in all_synsets if s.pos()=='n']
all_verbs = [s for s in all_synsets if s.pos()=='v']

funcs = wn.path_similarity, wn.lch_similarity, wn.wup_similarity
# requires ic
# wn.lin_similarity,  wn.jcn_similarity
lines = []
for i in range(1000):
    n1 = random.choice(all_nouns)
    n2 = n1
    if i%10>0:
        n2 = random.choice(all_nouns)
    line = [n1.name(),n2.name()]+[f(n1,n2) for f in funcs]
    lines.append(line)

for i in range(1000):
    v1 = random.choice(all_verbs)
    v2 = v1
    if i%10>0:
        v2 = random.choice(all_verbs)
    line = [v1.name(),v2.name()]+[f(v1,v2) for f in funcs]
    lines.append(line)

import csv
with open('fuzz.csv','wb') as f: 
    writer = csv.writer(f)
    for l in lines:
        writer.writerow(l)

from nltk.corpus.reader.wordnet import WordNetICCorpusReader
wnic = WordNetICCorpusReader(nltk.data.find('corpora/wordnet_ic'),'.*\.dat')
ic = wnic.ic('ic-brown.dat')
lines = []
funcs = wn.lin_similarity,  wn.jcn_similarity

nouns_in_ic = [s for s in all_synsets if s.pos()=='n' and s.offset() in ic['n']]
for i in range(1000):
    if i%7==0:
        n1 = random.choice(all_nouns)
    else:
        n1 = random.choice(nouns_in_ic)
    n2 = n1
    if i%11>0:
        if i%5 == 0:
            n2 = random.choice(all_nouns)
        else:
            n2 = random.choice(nouns_in_ic)
    line = [n1.name(),n2.name()]+[f(n1,n2,ic) for f in funcs]
    lines.append(line)

verbs_in_ic = [s for s in all_synsets if s.pos()=='v' and s.offset() in ic['v']]
for i in range(1000):
    if i%7==0:
        n1 = random.choice(all_verbs)
    else:
        n1 = random.choice(verbs_in_ic)
    n2 = n1
    if i%11>0:
        if i%5 == 0:
            n2 = random.choice(all_verbs)
        else:
            n2 = random.choice(verbs_in_ic)
    line = [v1.name(),v2.name()]+[f(v1,v2,ic) for f in funcs]
    lines.append(line)

import csv
with open('fuzz2.csv','wb') as f: 
    writer = csv.writer(f)
    for l in lines:
        writer.writerow(l)
