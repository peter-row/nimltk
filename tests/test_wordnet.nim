import unittest,tables
include nimltk.corpus.reader.wordnet

proc close(x:float,y:float):bool = 
  let diff = x-y
  if abs(diff)<1e-295: return true
  if x==Inf:
    if y>1e299: return true
  if y==Inf:
    if x>1e299: return true
  return false

suite "wordnet":
    test "lemmaIdxTest":
        for i in countup(0,1):
            let s = "acquisition n 4 3 @ ~ + 4 1 00077419 13253255 05752544 05637558"
            let lem = parseLemmaOffset(s)
            check lem.lemma == "acquisition"
            check lem.pos == "n"
            check lem.synset_offsets == @[00077419,13253255,05752544,05637558]

    test "NextSubStrTest":
        let buff = "test 1 two 3"
        var token = ""
        var pos1 = 0
        var pos0 = 0
        check next_word_range(pos0,pos1,buff)
        check buff[pos0..pos1-1] == "test"
        check next_word_range(pos0,pos1,buff)
        check buff[pos0..pos1-1] == "1"
        check next_word_range(pos0,pos1,buff)
        check buff[pos0..pos1-1] == "two"
        check next_word_range(pos0,pos1,buff)
        check buff[pos0..pos1-1] == "3"
        check next_word_range(pos0,pos1,buff)==false

    test "BuildLemmaIdx":
        #let OffsetMap = LemmaOffsetMap()
        #check synset_index(OffsetMap,".45_caliber.a.01")==3146895
        #let s = synset_line(synset_index(OffsetMap,".45_caliber.a.01"),"a")
        #echo s
        #discard parse_synset_line(s)
        #echo synset_line(synset_index(OffsetMap,"set.v.01"),"v")
        #discard parse_synset_line(synset_line(synset_index(OffsetMap,"set.v.01"),"v"))
        discard false
        
    test "BuildSynsets":
        #let OffsetMap = LemmaOffsetMap()
        var reader = newCorpusReader(expandTilde("~/nltk_data/corpora/wordnet/"))
        #reader.loadSynsetCache()
        let key = synset_index(reader.lemmaOffsets,"set.n.01")
        let synset = reader.synsetFromKey(key)#table[key.offset]
        check synset.name == "set.n.01"
        check synset.pos == Pos.noun
        check synset.definition == "a group of things of the same kind that belong together and are so used"
        check synset.lexname_id == 14
        check synset.lexname()=="noun.group"
        check synset.lemmas[0].lemma_name == "set"
        
    test "Distances":
        var reader = newCorpusReader(expandTilde("~/nltk_data/corpora/wordnet/"))
        let ic = readIC(expandTilde("~/nltk_data/corpora/wordnet_ic/ic-brown.dat"))
        check ic[Pos.noun].len() == 33255
        check ic[Pos.verb].len() == 11140
        check ic[Pos.noun][0]==1915712.0
        check ic[Pos.noun][11272198]==1.0
        check ic[Pos.noun][6946823]==1054.0
        check ic[Pos.verb][0]==2444712.0
        check ic[Pos.verb][1441793]==75.0
        check ic[Pos.verb][360092]==832.0
        let cat = reader.synset("cat.n.01")
        let dog = reader.synset("dog.n.01")
        check lcsIc(cat,dog,ic).ic1 ==9.040649895580986
        check lcsIc(cat,dog,ic).ic2 ==9.006014398918229
        check lcsIc(cat,dog,ic).ic_hyp == 7.911666509036577
        
        for line in lines("./tests/fuzz2.csv"):
          let L = line.split(",")
          check len(L) == 4
          let name1 = L[0]
          let name2 = L[1]
          check close(reader.synset(name1).linSimilarity(reader.synset(name2),ic),parseFloat(L[2]))
          check close(0.001+reader.synset(name1).linSimilarity(reader.synset(name2),ic),parseFloat(L[2]))==false
          check close(reader.synset(name1).jcnSimilarity(reader.synset(name2),ic),parseFloat(L[3]))
          if not close(reader.synset(name1).linSimilarity(reader.synset(name2),ic),parseFloat(L[2])):
            echo name1," ",name2, " ",L[2]," ", (reader.synset(name1).linSimilarity(reader.synset(name2),ic))
          if not close(reader.synset(name1).jcnSimilarity(reader.synset(name2),ic),parseFloat(L[3])):
            echo name1," ",name2, " ",L[3]," ", (reader.synset(name1).jcnSimilarity(reader.synset(name2),ic))
          
        let palo_alto = reader.synset("palo_alto.n.01")
        check palo_alto.maxDepth() == 9
        check palo_alto.minDepth() == 9
        
        var syn = reader.synset("fall.v.12")
        
        for line in lines("./tests/fuzz.csv"):
          let L = line.split(",")
          check len(L) == 5
          let name1 = L[0]
          let name2 = L[1]
          check reader.synset(name1).pathSimilarity(reader.synset(name2))==parseFloat(L[2])
          check reader.synset(name1).LchSimilarity(reader.synset(name2))==parseFloat(L[3])
          check reader.synset(name1).wupSimilarity(reader.synset(name2))==parseFloat(L[4])
          if not (reader.synset(name1).wupSimilarity(reader.synset(name2))==parseFloat(L[4])):
            echo name1," ",name2, " ",L[4]," ", (reader.synset(name1).wupSimilarity(reader.synset(name2)))
        
        check syn.related(hypernym).len==2
        check ((syn.related(hypernym)[0].name)=="fall.v.17")
        check ((syn.related(hypernym)[1].name)=="sin.v.01")
        check reader.synset("person.n.01").rootHypernyms()[0].name=="entity.n.01"
        check reader.synset("sail.v.01").rootHypernyms()[0].name=="travel.v.01"
        check reader.synset("fall.v.12").rootHypernyms()[1].name=="act.v.01"
        check reader.synset("fall.v.12").rootHypernyms()[0].name=="fall.v.17"
        check palo_alto.lowestCommonHypernyms(palo_alto,useMinDepth=true)[0].name=="palo_alto.n.01"
        let w = lowestCommonHypernyms(reader.synset("chef.n.01"),reader.synset("fireman.n.02"))[0]
        check w.name == "worker.n.01"
        
        check reader.synset("chef.n.01").shortest_path_distance(reader.synset("fireman.n.02"))==7
        check reader.synset("black.a.01").shortest_path_distance(reader.synset("slow.a.01"),simulate_root=true)==2
        check lowestCommonHypernyms(reader.synset("sail.v.01"),reader.synset("fireman.n.02")).len ==0
        
        check syn.allHypernyms().len == 8
        check syn.allHypernyms().len == 8 # check twice - it's cached
        check syn.reader.posMaxDepth(Pos.verb) == 13
        check syn.reader.posMaxDepth(Pos.verb) == 13
        check syn.reader.posMaxDepth(Pos.noun) == 19
        check syn.reader.posMaxDepth(Pos.noun) == 19
        let n1 = reader.synset("camelpox.n.01")
        let n2 = reader.synset("pygopus.n.01")
        check n1.lowestCommonHypernyms(n2).len == 1
        let subsumer = n1.lowestCommonHypernyms(n2)[0]
        check subsumer.name == "abstraction.n.06"
        check subsumer.maxDepth() == 1
        check n1.shortestPathDistance(subsumer) == 10
        check n2.shortestPathDistance(subsumer) == 6
        check n1.pathSimilarity(n2)==0.058823529411764705
        check n1.LchSimilarity(n2)==0.8043728156701697
        check n1.wupSimilarity(n2)==0.2
        
        let v1 = reader.synset("escalade.v.01")
        check v1.needsRoot()
        let v2 = reader.synset("capture.v.04")
        check v1.pathSimilarity(v2)==0.14285714285714285
        check v1.LchSimilarity(v2)==1.3121863889661687
        check v1.wupSimilarity(v2)==0.25
        
