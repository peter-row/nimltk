# nimltk

Currently contains a wrapper for the wordnet parser, and a python module to make
python's wordnet load a bit faster (requires PyNimFFI).

It's not as full featured as nltk's wordnet yet, but it contains the basic parser code.