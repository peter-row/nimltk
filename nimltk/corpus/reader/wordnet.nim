import strutils, critbits, os, streams, times, tables, intsets,sequtils,queues
import re, sets, hashes, math, algorithm

type
  Pos* {.pure.}= enum
    adj_sat, adj, adv, noun, verb
  
  SyntacticMarker* {.pure.}= enum
    none, p, a, ip
  
  Relation* = enum
    hypernym, instance_hypernym, hyponym, instance_hyponym,
    member_holonym, substance_holonym, part_holonym, member_meronym,
    substance_meronym, part_meronym, topic_domain, region_domain,
    usage_domain, topic_member, region_member, usage_member,
    attribute, entailment, causes, also_see, verb_group, 
    similar_to, derivationally_related, antonym, derived_from,
    particle_of
  
  LemmaIndex* = tuple
    lemma: string
    pos: string
    synset_offsets: seq[int]
  
  LemmaOffsets* = CritBitTree[seq[int]]
  
  SynsetRelation = tuple
    rel: Relation
    pos: Pos
    offset: int
  
  LemmaRelation = tuple
    rel: Relation
    sourceIdx: int
    targetPos: Pos
    targetOffset: int
    targetIdx: int
  
  Lemma* = tuple
    lemma_name: string
    lex_id: int
    syn_mark: SyntacticMarker
    frame_ids: IntSet
  
  SynsetObj = tuple
    reader: ref CorpusReader
    name: string # e.g. "set"
    pos: Pos # "v"
    offset: int # 1494310
    frame_ids: IntSet # 21
    lemmas: seq[Lemma]
    definition: string
    examples: seq[string]
    lexname_id: int
    pointers: seq[SynsetRelation]
    lemma_pointers: seq[LemmaRelation]
  
  Synset* = ref SynsetObj
  
  SynsetCache = Table[Pos,ref Table[int,Synset]]
  
  SynsetKey = tuple
    pos:Pos
    offset:int
  
  CorpusReader* = tuple
    root: string
    synsetCache: SynsetCache
    lemmaOffsets: LemmaOffsets
    DataFiles: Table[Pos,File]
    lexnames: seq[string]
    allHypernymCache: Table[SynsetKey,seq[SynsetKey]]
    minDepthCache: Table[SynsetKey,int]
    maxDepthCache: Table[SynsetKey,int]
    posMaxDepthCache: Table[Pos,int]
    fakeRoots: Table[Pos,Synset]
    loaded: bool
    
  IC* = Table[Pos, ref Table[int,float]]
  LcsIc = tuple[ic1: float, ic2: float, ic_hyp:float]

proc invertTable[A,B](table:Table[A,B]):Table[B,A]=
  result = initTable[B, A]()
  for k in table.keys():
    result[table[k]] = k
  assert result.len == table.len

#const pos_from_i = {0:adj, 1:adv, 2:noun, 3:verb}.toTable
#const i_from_pos = {adj:0, adv:1, noun:2, verb:3}.toTable

const suffix_from_pos = [(Pos.adj, "adj"), (Pos.adv, "adv"),(Pos.noun,"noun"),(Pos.verb,"verb")].toTable
const pos_from_suffix = invertTable(suffix_from_pos)

const s_from_pos = [(Pos.adj, "a"),(Pos.adjSat, "s"),(Pos.adv, "r"),
                    (Pos.noun,"n"),(Pos.verb,"v")].toTable

const pos_from_s = invertTable(s_from_pos)

const s_from_relation = [(hypernym,"@"),
                       (instance_hypernym,"@i"),
                       (hyponym,"~"),
                       (instance_hyponym,"~i"),
                       (member_holonym,"#m"),
                       (substance_holonym,"#s"),
                       (part_holonym,"#p"),
                       (member_meronym,"%m"),
                       (substance_meronym,"%s"),
                       (part_meronym,"%p"),
                       (topic_domain,";c"),
                       (region_domain,";r"),
                       (usage_domain,";u"),
                       (topic_member,"-c"),
                       (region_member,"-r"),
                       (usage_member,"-u"),
                       (attribute,"="),
                       (entailment,"*"),
                       (causes,">"),
                       (also_see,"^"),
                       (verb_group,"$"),
                       (similar_to,"&"),
                       (derivationally_related,"+"),
                       (antonym,"!"),
                       (derived_from,"\\"),
                       (particle_of,"<")
                       ].toTable

const relation_from_s = invertTable(s_from_relation)

proc lexname(s:Synset):string =
  s.reader.lexnames[s.lexname_id]

proc `==`*(s1:Synset,s2:Synset): bool {.inline.} =
  (s1.pos == s2.pos) and (s1.offset == s2.offset)

proc needsRoot(s:Synset):bool {.inline.} =
  (s.pos == Pos.verb)

#or (s.pos == Pos.noun and s.reader.version == 1_6)

proc synsetFromKey(reader: ref CorpusReader, k: SynsetKey): Synset {.inline.}

proc getKey(s:Synset):SynsetKey {.inline.} =
  (s.pos,s.offset)

proc fakeRoot(s:Synset): Synset=
  s.reader.fakeRoots[s.pos]

proc related*(s:Synset,relation:Relation):seq[Synset] {.inline.}=
  var len = 0
  for r in s.pointers:
    let (rel, pos, offset) = r
    if (rel == relation):
      len += 1
  result = newSeq[Synset](len)
  var i = 0
  for r in s.pointers:
    let (rel, pos, offset) = r
    if (rel == relation):
      let k: SynsetKey = (pos,offset)
      result[i]=synsetFromKey(s.reader,k)
      inc(i)

iterator related*(s:Synset, relation:Relation):Synset {.inline.} =
  for r in s.pointers:
    let (rel, pos, offset) = r
    if (rel == relation):
      yield synsetFromKey(s.reader,(pos,offset))

iterator hypers*(s:Synset):Synset {.inline.} =
  for r in s.pointers:
    let (rel, pos, offset) = r
    if (rel == hypernym) or (rel == instance_hypernym):
      yield synsetFromKey(s.reader,(pos,offset))

proc rootHypernyms(s:Synset,seen:var HashSet[SynsetKey]):seq[Synset] =
  ## recursively get root hypernyms and instance_hypernyms
  result = @[]
  seen.incl(s.getKey())
  var hypernyms = s.related(hypernym)
  hypernyms.add(s.related(instance_hypernym))
  if len(hypernyms)>0:
    for h in hypernyms:
      if not (h.getKey() in seen):
        result.add(rootHypernyms(h,seen))
  else:
    result.add(s)

proc rootHypernyms*(s:Synset):seq[Synset] =
  var seen = initSet[SynsetKey]()
  rootHypernyms(s,seen)

proc allHypernymKeys(s:Synset,seen:var HashSet[SynsetKey])=
  let k = getKey(s)
  if (k in s.reader.allHypernymCache):
    for h in s.reader.allHypernymCache[k]:
      if not (h in seen):
        seen.incl(h)
    return
  seen.incl(s.getKey())
  for h in hypers(s):
    let k = h.getKey()
    if not (k in seen):
      seen.incl(k)
      allHypernymKeys(h,seen)

proc allHypernymKeys(s:Synset):seq[SynsetKey] =
  var seen = initSet[SynsetKey]()
  allHypernymKeys(s,seen)
  let r = s.reader
  let res = toSeq(seen.items())
  s.reader.allHypernymCache[getKey(s)] = res
  res

proc allHypernyms(s:Synset):seq[Synset] =
  let res = allHypernymKeys(s)
  res.map(proc (k:SynsetKey):Synset = s.reader.synsetFromKey(k))

proc minDepth*(s:Synset): int=
  let k = getKey(s)
  if likely(s.reader.minDepthCache.hasKey(k)):
    return s.reader.minDepthCache[k]
  var found = false
  for h in s.hypers():
    if likely(found):
      result = min(result, h.minDepth()+1)
    else:
      found = true
      result = h.minDepth()+1
  if not found:
    result = 0
  s.reader.minDepthCache[k] = result

proc maxDepth*(s:Synset): int=
  let k = getKey(s)
  if likely(s.reader.maxDepthCache.hasKey(k)):
    return s.reader.maxDepthCache[k]
  var found = false
  for h in s.hypers():
    if likely(found):
      result = max(result, h.maxDepth()+1)
    else:
      found = true
      result = h.maxDepth()+1
  if not found:
    result = 0
  s.reader.maxDepthCache[k] = result

proc commonHypernymKeys(s1:Synset, s2:Synset):seq[SynsetKey]=
  var hypernyms1 = allHypernymKeys(s1)
  var hypernyms2 = allHypernymKeys(s2)
  toSeq(intersection(hypernyms1.toSet(),hypernyms2.toSet()).items())

proc commonHypernyms*(s1:Synset, s2:Synset):seq[Synset] = 
  let r = s1.reader
  commonHypernymKeys(s1,s2).map(proc (k:SynsetKey):Synset = r.synsetFromKey(k))

proc lowestCommonHypernyms(s1:Synset, s2:Synset,simulateRoot:bool=false,useMinDepth:bool=false):seq[Synset]=
  var commonHypernyms = commonHypernyms(s1,s2)
  if simulateRoot:
    commonHypernyms.add(s1.fakeRoot())
  if (commonHypernyms.len() > 0):
    if useMinDepth:
      let max_depth = max(commonHypernyms.map(proc (s:Synset):int = s.minDepth()))
      result = filter(commonHypernyms, proc(s: Synset): bool = s.minDepth()==max_depth)
    else:
      let max_depth = max(commonHypernyms.map(proc (s:Synset):int = s.maxDepth()))
      result = filter(commonHypernyms, proc(s: Synset): bool = s.maxDepth()==max_depth)
    sort(result,proc (x, y: Synset):int = system.cmp(x.name,y.name))
  else:
    result = @[]

proc shortestHypernymPaths(s: Synset,simulateRoot:bool):Table[SynsetKey,int] =  
  type SynKeyInt = tuple[k:SynsetKey,i:int]
  # simulated root:
  if s.offset == -1:
    let res = (s.getKey(),0)
    return @[res].toTable()
  
  let reader = s.reader
  result = initTable[SynsetKey,int]()
  var queue = initQueue[SynKeyInt]()
  queue.add((s.getKey(),0))
  while queue.len>0:
    let synkeyDepth = queue.dequeue()
    let k =synkeyDepth.k
    let depth = synkeyDepth.i
    let s_i = reader.synsetFromKey(k)
    if k in result:
      continue
    result[k] = depth
    for hyp in s_i.hypers(): # related(instance_hypernym):
      queue.add((hyp.getKey(), depth + 1))
  if simulateRoot:
    let fake_synset = s.fakeRoot()
    let values = toSeq(result.values())
    result[fake_synset.getKey()] = max(values) + 1

proc shortestPathDistance(s1: Synset, s2:Synset, simulateRoot:bool=false): int =
  if s1 == s2:
    return 0
  let dists1 = shortestHypernymPaths(s1,simulateRoot)
  let dists2 = shortestHypernymPaths(s2,simulateRoot)
  result = high(int)
  for s, d in dists1.pairs():
    if s in dists2:
      result = min(result,d+dists2[s])

proc pathSimilarity(s1: Synset, s2:Synset, simulateRoot:bool=true): float =
  let distance = shortestPathDistance(s1, s2, simulateRoot = simulateRoot and s1.needsRoot)
  1.0 / (toFloat(distance) + 1.0)

proc posMaxDepth(r: ref CorpusReader,pos:Pos):int

proc LchSimilarity(s1: Synset, s2:Synset, simulateRoot:bool=true): float =
  let depth:int = s1.reader.posMaxDepth(s1.pos)
  let distance = shortestPathDistance(s1, s2, simulateRoot = simulateRoot and s1.needsRoot)
  return -ln(toFloat(distance + 1) / toFloat(2 * depth))

proc wupSimilarity(s1: Synset, s2:Synset, simulateRoot:bool=true,useMinDepth:bool=true): float =
  let needsRoot = s1.needsRoot()
  let subsumers = s1.lowestCommonHypernyms(s2, simulateRoot=simulateRoot and needsRoot, useMinDepth=useMinDepth)
  if subsumers.len==0:
    return 0.0
  let subsumer = subsumers[0]
  let depth = subsumer.maxDepth() + 1
  let len1 = s1.shortestPathDistance(subsumer, simulateRoot=simulateRoot and needsRoot)
  let len2 = s2.shortestPathDistance(subsumer, simulateRoot=simulateRoot and needsRoot)
  result = (2.0 * toFloat(depth)) / (2.0* toFloat(depth) + toFloat(len1) + toFloat(len2))

proc getIC(s:Synset,ic:IC): float {.inline.} = 
  if unlikely(not ic.hasKey(s.pos)):
    raise newException(ValueError,"No ic for POS "&repr(s.pos))
  if ic[s.pos].hasKey(s.offset):
    result = -ln(ic[s.pos][s.offset] / ic[s.pos][0])
  else: 
    result = Inf

proc lcsIc(s1:Synset, s2:Synset, ic:IC): LcsIc =
  if unlikely(s1.pos != s2.pos):
    raise newException(ValueError,"Synsets must have the same POS")
  result.ic1 = getIC(s1,ic)
  result.ic2 = getIC(s2,ic)
  let subsumers = s1.commonHypernyms(s2)
  result.ic_hyp = 0
  if subsumers.len>0:
    result.ic_hyp = max(subsumers.map(proc(s:Synset):float = getIC(s,ic)))

proc jcnSimilarity(s1: Synset, s2:Synset, ic:IC): float =
  if s1 == s2: 
    return Inf
  let lcs = lcsIc(s1,s2,ic)
  let ic1 = lcs.ic1
  let ic2 = lcs.ic2
  let ic_hyp = lcs.ic_hyp
  if ic1 == 0 or ic2 == 0:
    return 0
  let ic_difference = ic1 + ic2 - (2 * ic_hyp)
  if ic_difference == 0:
    return Inf
  return 1 / ic_difference

proc linSimilarity(s1: Synset, s2:Synset, ic:IC): float =
  if s1 == s2: 
    return 1.0
  let lcs = lcsIc(s1,s2,ic)
  return (2.0 * lcs.ic_hyp) / (lcs.ic1 + lcs.ic2)

proc next_word_range*(pos0: var int, pos1: var int, buff: string): bool {.inline.} =
  pos0 = pos1
  while (buff[pos0] in Whitespace) and buff[pos0]!='\0':
    inc(pos0)
  if buff[pos0]=='\0':
    return false
  pos1 = pos0
  inc(pos1)
  while not (buff[pos1] in Whitespace) and buff[pos1]!='\0':
    inc(pos1)
  return true

proc next_token(pos0: var int, pos1:var int, buff:string): string {.inline.} =
  discard next_word_range(pos0,pos1,buff)
  result = buff[pos0..pos1-1]

proc parseLemmaOffset*(line: string): LemmaIndex {.inline.} =
  var pos0 = 0
  var pos1 = 0
  # get the lemma
  let lemma = next_token(pos0,pos1,line)
  let part_of_speech = next_token(pos0,pos1,line)[0..1]

  # get the number of synsets for this lemma
  let n_synsets = parseInt(next_token(pos0,pos1,line))
  doAssert n_synsets > 0
  
  # get the pointer symbols for all synsets of this lemma
  let n_pointers = parseInt(next_token(pos0,pos1,line))
  for i in countup(1,n_pointers):
    doAssert next_word_range(pos0,pos1,line)
  # same as number of synsets
  let n_senses = parseInt(next_token(pos0,pos1,line))
  assert n_synsets == n_senses
  
  # get number of senses ranked according to frequency
  doAssert next_word_range(pos0,pos1,line)

  # get synset offsets
  var synset_offsets: seq[int]
  synset_offsets.newSeq(n_synsets)
  for i in countup(0,n_synsets-1):
    synset_offsets[i] = parseInt(next_token(pos0,pos1,line))
  assert next_word_range(pos0,pos1,line) == false
  result.lemma = lemma
  result.pos = part_of_speech
  result.synset_offsets = synset_offsets

proc LemmaKey(lemma:string,pos:string):string{.inline raises:[].} =
  result = (lemma & "\n") & pos

proc LoadLemmaOffsetMap*(reader: ref CorpusReader) =
  for suffix in @["adj", "adv", "verb", "noun"]:
    let fname = joinPath(reader.root,"index.")&suffix
    var lemma: LemmaIndex
    for line in lines(fname):
      if line[0] == ' ':
        continue
      lemma = parseLemmaOffset(line)
      let key = LemmaKey(lemma.lemma,lemma.pos)
      doAssert containsOrIncl(reader.lemmaOffsets,key,lemma.synset_offsets)==false

proc synset_index*(offsets:LemmaOffsets,name:string):SynsetKey =
  let parts = split(name,".")
  let i = parseInt(parts[parts.len-1])-1
  let pos = parts[parts.len-2]
  let syn_name = join(parts[0..(parts.len-3)],".")
  let k = LemmaKey(syn_name,pos)
  if unlikely(offsets.hasKey(k)==false):
    raise newException(IndexError,"No such synset")
  let synsets = offsets[k]
  if unlikely(i>=synsets.len):
    raise newException(IndexError,"No such synset at i")
  result.pos = pos_from_s[pos]
  result.offset = synsets[i]

proc synset_line*(offset:int,pos:string,root:string):string =
  let suffix = suffix_from_pos[pos_from_s[pos]]
  let fname = joinPath(root,"data.")&suffix
  var s = newFileStream(fname, fmRead)
  s.setPosition(offset)
  return s.readLine()

let lemma_re = re"(.*?)(\(.*\))?$"

proc parse_synset_line*(reader: ref CorpusReader, line:string):Synset =
  var pos0 = 0
  var pos1 = 0
  result = new SynsetObj
  # synset
  result.offset = parseInt(next_token(pos0,pos1,line))
  result.lexname_id = parseInt(next_token(pos0,pos1,line))
  let pos_str = next_token(pos0,pos1,line)
  result.pos = pos_from_s[pos_str]
  result.frame_ids = initIntSet()
  
  # lemmas
  let n_lemmas = parseHexInt(next_token(pos0,pos1,line))
  result.lemmas.newSeq(n_lemmas)
  var matches: array[2, string]
  for i in countup(0, n_lemmas-1):
    let lemma_name_marker = next_token(pos0,pos1,line)
    doAssert match(lemma_name_marker, lemma_re, matches)
    result.lemmas[i].lemma_name = matches[0]
    let syn_mark = matches[1]
    case syn_mark
      of nil:
        result.lemmas[i].syn_mark = SyntacticMarker.none
      of "(p)":
        result.lemmas[i].syn_mark = SyntacticMarker.p
      of "(a)":
        result.lemmas[i].syn_mark = SyntacticMarker.a
      else:
        assert syn_mark == "(ip)"
        result.lemmas[i].syn_mark = SyntacticMarker.ip
    result.lemmas[i].lex_id = parseHexInt(next_token(pos0,pos1,line))
    result.lemmas[i].frame_ids = initIntSet()
  
  # relations
  let n_pointers = parseInt(next_token(pos0,pos1,line))
  result.pointers = @[]
  result.lemma_pointers = @[]
  for i in countup(0,n_pointers-1):
    let rel_s = next_token(pos0,pos1,line)
    assert (relation_from_s.hasKey(rel_s))
    let rel = relation_from_s[rel_s]
    let offset = parseInt(next_token(pos0,pos1,line))
    let pos: Pos = pos_from_s[next_token(pos0,pos1,line)]
    let lemma_ids_str = next_token(pos0,pos1,line)
    # add to the tables
    if lemma_ids_str == "0000":
      let synsetRelation: SynsetRelation = (rel:rel, pos:pos, offset:offset)
      if (result.pointers.contains(synsetRelation)==false):
        result.pointers.add(synsetRelation)
    else:
      let sourceIdx = parseHexInt(lemma_ids_str[0..2])-1
      let targetIdx = parseHexInt(lemma_ids_str[2..4])-1
      let lemmaRelation: LemmaRelation = (rel:rel, sourceIdx:sourceIdx,
              targetPos:pos,targetOffset:offset,targetIdx:targetIdx)
      if (result.lemma_pointers.contains(lemmaRelation)==false):
          result.lemma_pointers.add(lemmaRelation)
  # frames
  let frame_count_str = next_token(pos0,pos1,line)
  if frame_count_str != "|":
    let frame_count = parseInt(frame_count_str)
    for i in countup(0,frame_count-1):
      doAssert next_token(pos0,pos1,line) == "+"
      let frame_id = parseInt(next_token(pos0,pos1,line))-1
      let lemma_id = parseHexInt(next_token(pos0,pos1,line))-1
      if lemma_id == -1:
        result.frame_ids.incl(frame_id)
        for i in countup(0, n_lemmas-1):
          result.lemmas[i].frame_ids.incl(frame_id)
      else:
        result.lemmas[lemma_id].frame_ids.incl(frame_id)
    doAssert next_token(pos0,pos1,line) == "|"
  
  var definitions: seq[string]
  definitions.newSeq(0)
  result.examples.newSeq(0)
  for gloss_unstripped in (line[pos1..line.len]).split(';'):
    let gloss = gloss_unstripped.strip()
    if gloss[0]=='"':
      result.examples.add(gloss.strip(chars={'"'}))
    else:
      definitions.add(gloss)
  result.definition=definitions.join("; ")
  let name=result.lemmas[0].lemma_name.toLower()
  
  let lemmaKey = case result.pos
  of Pos.adjSat: LemmaKey(name,s_from_pos[Pos.adj])
  else: LemmaKey(name,pos_str)
  
  let offsets = reader.lemmaOffsets[lemmaKey]
  var senseIdx = 0
  var found = false
  for k in offsets:
    inc(senseIdx)
    if k == result.offset:
      found = true
      break
  result.name = name&"."&pos_str&"."&intToStr(senseIdx,2)
  if not found:
    raise newException(IndexError,"Unable to find sense of "&repr(result)&" from "&line)
  result.reader = reader

proc synsetFromKey(reader: ref CorpusReader, k: SynsetKey): Synset=
  if likely(reader.synsetCache[k.pos].hasKey(k.offset)):
    result = reader.synsetCache[k.pos][k.offset]
  else:
    let f = reader.DataFiles[k.pos]
    f.setFilePos(k.offset)
    result = parse_synset_line(reader,f.readLine())
    reader.synsetCache[k.pos][k.offset]=result

proc loadSynsetCache*(reader: ref CorpusReader) =
  if reader.loaded:
    return
  var synsetCache = initTable[Pos,ref Table[int,Synset]]()
  let t0 = cpuTime()
  for pos_tag in suffix_from_pos.keys():
    let suffix = suffix_from_pos[pos_tag]
    let fname = joinPath(reader.root,"data.")&suffix
    var table = initTable[int,Synset]()
    for line in lines(fname):
      if line.startswith(" "):
        continue
      var synset = parse_synset_line(reader,line)
      table[synset.offset]=synset
    var table_ref = new(Table[int,Synset])
    table_ref[] = table
    synsetCache[pos_tag]=table_ref
  reader.synsetCache = synsetCache
  reader.loaded = true

proc posMaxDepth(r: ref CorpusReader,pos:Pos):int = 
  if likely(r.posMaxDepthCache.hasKey(pos)):
    return r.posMaxDepthCache[pos]
  r.loadSynsetCache()
  result = 0
  for s in r.synsetCache[pos].values:
    result = max(result, s.maxDepth)
  assert r.fakeRoots[pos].pos == pos
  if r.fakeRoots[pos].needsRoot:
    result += 1
  r.posMaxDepthCache[pos] = result

proc loadLexnames(reader: ref CorpusReader) =
  var lexnames:seq[string] = @[]
  var i = 0
  for line in lines(joinPath(reader.root,"lexnames")):
    let items = line.split()
    assert (parseInt(items[0]) == i)
    lexnames.add(items[1])
    inc(i)
  reader.lexnames=lexnames

proc newCorpusReader(root:string): ref CorpusReader =
  result = new(CorpusReader)
  result.root = root
  LoadLemmaOffsetMap(result)
  loadLexnames(result)
  result.allHypernymCache = initTable[SynsetKey,seq[SynsetKey]]()
  result.minDepthCache = initTable[SynsetKey,int]()
  result.maxDepthCache = initTable[SynsetKey,int]()
  result.synsetCache = initTable[Pos,ref Table[int,Synset]]()
  result.fakeRoots = initTable[Pos,Synset](8)
  result.posMaxDepthCache = initTable[Pos,int]()
  # make fake roots
  for pos in [Pos.adj, Pos.adv, Pos.adj_sat, Pos.noun, Pos.verb]:
    result.synsetCache[pos]=newTable[int,Synset]()
    var fakeRoot: Synset
    fakeRoot = new SynsetObj
    fakeRoot.offset = -1
    fakeRoot.pos = pos
    fakeRoot.reader = result
    fakeRoot.name = "Fake Root "& repr(pos)
    result.minDepthCache[fakeRoot.getKey()] = 0
    result.maxDepthCache[fakeRoot.getKey()] = 0
    result.fakeRoots[pos] = fakeRoot
  
  result.DataFiles = initTable[Pos,File]()
  for pos in [Pos.adj, Pos.adv, Pos.noun, Pos.verb]:
    var f: File
    let fname = joinPath(result.root,"data.")&suffix_from_pos[pos]
    doAssert open(f,fname)
    result.DataFiles[pos]=f

proc unloadCorpusReader(reader: ref CorpusReader) = 
  var no_offsets: CritBitTree[seq[int]]
  reader.lemmaOffsets = no_offsets
  reader.synsetCache = initTable[Pos, ref Table[int,Synset]](0)
  reader.root = ""
  reader.loaded = false

proc readIC(icfile:string):IC =
  result = initTable[Pos,ref Table[int,float]]()
  result[Pos.noun]=newTable[int,float]()
  result[Pos.verb]=newTable[int,float]()
  var linenum = -1
  for line in lines(icfile):
    inc(linenum)
    if linenum>0:
      let fields = line.split(" ")
      let offset = parseInt(fields[0][0..fields[0].high-1])
      let pos = pos_from_s[fields[0][fields[0].high..fields[0].high+1]]
      let value = parseFloat(fields[1])
      if value == 0.0:
        continue
      if len(fields) == 3 and fields[2] == "ROOT":
        result[pos][0] = result[pos][0] + value
      result[pos][offset]=value

proc synset(reader:ref CorpusReader, n:string):Synset =
  let key = synset_index(reader.lemmaOffsets,n)
  synsetFromKey(reader, key)

when appType == "lib":
  import pyNimFFI, msgpack
  type lemmaKV = tuple[lemma:string,pos:string,val:seq[int]]
  
  pyClass CorpusReader:
  
    proc offset_map_items(self: ref CorpusReader): bstring =
      var res: seq[lemmaKV]= @[]
      res.setLen(self.lemmaOffsets.len)
      var i = 0;
      for pair in self.lemmaOffsets.pairs():
        let keys = pair.key.split("\n")
        let lemma = keys[0]
        let pos = keys[1]
        res[i] = (lemma,pos,pair.val)
        inc(i)
      var s_result:string = pack(res)
      result.len = s_result.len
      result.buffer = s_result
  
  proc corpus_reader(root:cstring): ref CorpusReader {.pyFuncSafe.} =
    newCorpusReader($root)

