# include libs
{.passC:"-DU_DISABLE_RENAMING=1".}
{.passL: "-licui18n -licuuc -licudata".}

const
  ubrk = "<unicode/ubrk.h>"
  utext = "<unicode/utext.h>"

type
  UBreakIterator {.importc: "UBreakIterator*", header:ubrk.} = distinct pointer
  UBreakIteratorType = distinct cint
  UErrorCode = int32
  UText {.importc: "UText*", header:utext.} = distinct pointer
  UChar {.importc: "UChar*", header:utext.} = distinct pointer
  UBreakRuleStatus = distinct int32
  ICUIterator* = tuple
    ubrk: UBreakIterator
    utext: UText
    status: UErrorCode
  
  ICUError* = object of Exception

const
  UBRK_CHARACTER: UBreakIteratorType = 0.UBreakIteratorType
  UBRK_WORD: UBreakIteratorType = 1.UBreakIteratorType
  UBRK_LINE: UBreakIteratorType = 2.UBreakIteratorType
  UBRK_SENTENCE: UBreakIteratorType = 3.UBreakIteratorType
  
  UBRK_DONE: int32 = -1
  
  UBRK_WORD_NONE: UBreakRuleStatus = 0.UBreakRuleStatus

proc ubrk_close(boundary: UBreakIterator) {.importc: "ubrk_close", header:ubrk.}
proc ubrk_open(breakType:UBreakIteratorType,locale:cstring,text:UChar,textLen:int32,status: ptr UErrorCode):UBreakIterator {.importc: "ubrk_open", header:ubrk.}
proc ubrk_first(boundary: UBreakIterator):int32 {.importc: "ubrk_first", header:ubrk.}
proc ubrk_next(boundary: UBreakIterator):int32 {.importc: "ubrk_next", header:ubrk.}
proc ubrk_setUText(boundary: UBreakIterator,text:UText,status: ptr UErrorCode) {.importc: "ubrk_setUText", header:ubrk.}
proc ubrk_getRuleStatus(boundary: UBreakIterator):UBreakRuleStatus {.importc: "ubrk_getRuleStatus", header:ubrk.}

proc utext_close(o: UText) {.importc: "utext_close", header:utext.}
proc utext_openUTF8(t: UText, text:cstring, textLen:int32, status: ptr UErrorCode):UText {.importc: "utext_openUTF8", header:utext.}

proc u_errorName(e:UErrorCode):cstring {.importc: "u_errorName", header:utext.}
proc U_SUCCESS(e:UErrorCode):bool {.importc: "U_SUCCESS", header:utext.}

proc check(status:UErrorCode) =
  if not U_SUCCESS(status):
    raise newException(ICUError, $u_errorName(status))

proc ICUSentenceIterator*(locale="en"):ICUIterator =
  result.status = 0
  result.ubrk = ubrk_open(UBRK_SENTENCE,locale, nil.UChar,0, addr(result.status))
  result.status.check()

proc ICUWordIterator*(locale="en"):ICUIterator =
  result.status = 0
  result.ubrk = ubrk_open(UBRK_WORD,locale, nil.UChar,0, addr(result.status))
  result.status.check()

iterator iterIdx*(it:var ICUIterator, s:string): (int,int) {.inline.} =
  let ubrk = it.ubrk
  it.utext = utext_openUTF8(it.utext,s,s.len().int32,addr(it.status))
  it.status.check()
  ubrk.ubrk_setUText(it.utext,addr(it.status))
  it.status.check()
  var start = ubrk_first(ubrk).int
  var last = ubrk_next(ubrk).int
  while last != UBRK_DONE:
    yield (start,last)
    start = last
    last = ubrk_next(ubrk).int

iterator iterValues*(it:var ICUIterator, s:string) {.inline.} =
  for start, last in it.iterIdx(s):
    yield s[start..last-1]

proc isWord*(it: var ICUIterator):bool {.inline.} =
  let status = ubrk_getRuleStatus(it.ubrk)
  status.int !=UBRK_WORD_NONE.int

{.experimental.}
proc `=destroy`(o: var ICUIterator) =
  if o.utext.pointer != nil:
    utext_close(o.utext)
  if o.ubrk.pointer != nil: 
    ubrk_close(o.ubrk)

when isMainModule:
  let s="狭义上，“漢語”這個詞，仅指現代標準漢語——以北京話为标准语音、以官话为基础的現代白话文著作作为语法规範。在非表音情況下，僅指現代白話文的書面語。大中华地区的中小學中教授漢語的文字、語法、文學等的科目(例如“語文課、中文課、國文課”等，都是“中國語文科”的意思。中華民國和中華人民共和國的語文課，以普通話(中華民國稱國語)授課；在香港和澳門因通行粵語，所以學校會以粵語授課。中華人民共和國的普通話、中華民國的國語及新馬地區的华語大體上是相同的，只在個別字詞的讀音上有些微區別。此外，臺灣、香港和澳門是以繁体中文為主要文字的地区。"
  var sent_iter = ICUSentenceIterator("cn")
  var word_iter= ICUWordIterator("cn")
  for line in sent_iter.iterValues(s):
    echo "sentence: ",line
    for word in word_iter.iterValues(line):
      if word_iter.isWord():
        echo word
