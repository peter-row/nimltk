import perceptron,tables,sequtils,strutils,random
import nimprof
type
  PerceptronTagger*[T] = tuple
    perceptron: Perceptron[string,T]
    
  FeatureSet = Table[string,float]

# var trainer:PerceptronTrainer[string,Pos]  = initTrainer[string,Pos](tags)
#proc makeTagset[T](var tagger:PerceptronTagger[T])=
#  tagger.tagSet.initTable[string,T]()

proc update[T](trainer:var PerceptronTrainer[string,T], guess, truth, featureSet) =
  perceptron.update(trainer,featureSet,guess,truth)

proc normalize(word:string):string=
  #if ('-' in word) and (word[0] != '-'):
  #  return "!HYPHEN"
  if word.allCharsInSet(Digits) and len(word) == 4:
    return "!YEAR"
  elif word.allCharsInSet(Digits):
    return "!DIGITS"
  else:
    return word.toLower()

proc initFeatureSet(prevTag:string,prevTag2:string,all_words:seq[string],i:int):FeatureSet =
  let isCap = all_words[i+2][0].toUpper == all_words[i+2][0]
  let hasHyphen = ('-' in all_words[i+2])
  let prevWord2 = normalize(all_words[i])
  let prevWord = normalize(all_words[i+1])
  let currWord = normalize(all_words[i+2])
  let nextWord = normalize(all_words[i+3])
  let nextWord2 = normalize(all_words[i+4])
  {
    "bias-":1.0,
    "hasHyphen-"&($hasHyphen):1.0,
    "isCap-"&($isCap):1.0,
    #"prefix-"&currWord[0]:1.0,
    "prefix2-"&currWord[0..2]:1.0,
    "suffix-"&currWord[currWord.high-3..currWord.high]:1.0,
    "suffixP-"&prevWord[prevWord.high-3..prevWord.high]:1.0,
    "suffixN-"&nextWord[nextWord.high-3..nextWord.high]:1.0,
    #"suffix2-"&currWord[currWord.high-2..currWord.high]:1.0,
    #"suffix2P-"&prevWord[prevWord.high-2..prevWord.high]:1.0,
    #"suffix2N-"&nextWord[nextWord.high-2..nextWord.high]:1.0,
    #"suffix4-"&currWord[currWord.high-4..currWord.high]:1.0,
    #"suffix4P-"&prevWord[prevWord.high-4..prevWord.high]:1.0,
    #"suffix4N-"&nextWord[nextWord.high-4..nextWord.high]:1.0,
    "tagP-"&prevTag:1.0,
    "tagP2-"&prevTag2:1.0,
    "tags-"&prevTag&"-"&prevTag2:1.0,
    "word-"&currWord:1.0,
    "wordP-"&prevWord:1.0,
    "wordP2-"&prevWord2:1.0,
    "wordN-"&nextWord:1.0,
    "wordN2-"&nextWord2:1.0,
    "tagword-"&prevTag&"-"&currWord:1.0
  }.toTable()

# reversed tagger
# combined tagger

proc trained[T](sentences0:seq[tuple[words:seq[string],tags:seq[T]]],tags:seq[T],n_iter:int):PerceptronTagger[T]=
  #tagger.tagset = makeTagset(tagger)
  var trainer:PerceptronTrainer[string,T] = initTrainer[string,T](tags)
  var trainer2:PerceptronTrainer[string,T] = initTrainer[string,T](tags)
  echo "training ",len(sentences0)," sentences"
  var sentences = sentences0
  for iteration in 1..n_iter:
    shuffle(sentences)
    var c = 0
    var n = 0
    for sentence in sentences:
      var forwards: seq[T] = @[]
      let words = sentence.words
      let tags = sentence.tags
      var i = 0
      let all_words = concat(@["-START-","-START2-"],words,@["-END-", "-END2-"])
      var guess = "-START-"
      var prevTag = "-START2-"
      var prevTag2: string
      for i in 0..all_words.high-4:
        prevTag2 = prevTag
        prevTag = guess
        let featureSet = initFeatureSet(prevTag,prevTag2,all_words, i)
        let truth = tags[i]
        # if easy?
        guess = predict(trainer.perceptron,featureSet)
        if guess != truth:
          trainer.update(guess,truth,featureSet)
        else:
          c+=1
        n+=1
    echo c," of ",n, " ",c.float/n.float*100,"%"
  trainer.averageWeights()
  result.perceptron = trainer.perceptron

proc test[T](tagger:PerceptronTagger[T],sentences:seq[tuple[words:seq[string],tags:seq[T]]])=
  echo "testing ",len(sentences)," sentences"
  var c = 0
  var n = 0
  for sentence in sentences:
    let words = sentence.words
    let tags = sentence.tags
    var i = 0
    let all_words = concat(@["-START-","-START2-"],words,@["-END-", "-END2-"])
    var guess = "-START-"
    var prevTag = "-START2-"
    var prevTag2: string
    for i in 0..all_words.high-4:
      prevTag2 = prevTag
      prevTag = guess
      let featureSet = initFeatureSet(prevTag,prevTag2,all_words, i)
      let truth = tags[i]
      # if easy?
      n+=1
      guess = predict(tagger.perceptron,featureSet)
      if guess == truth:
        c+=1
  echo c," of ",n, " ",c.float/n.float*100,"%"

let wsj0 = ("Pierre|NNP Vinken|NNP ,|, 61|CD years|NNS old|JJ ,|, will|MD "&
              "join|VB the|DT board|NN as|IN a|DT nonexecutive|JJ director|NN "&
              "Nov.|NNP 29|CD .|.\nMr.|NNP Vinken|NNP is|VBZ chairman|NN of|IN "&
              "Elsevier|NNP N.V.|NNP ,|, the|DT Dutch|NNP publishing|VBG "&
              "group|NN .|. Rudolph|NNP Agnew|NNP ,|, 55|CD years|NNS old|JJ "&
              "and|CC former|JJ chairman|NN of|IN Consolidated|NNP Gold|NNP "&
              "Fields|NNP PLC|NNP ,|, was|VBD named|VBN a|DT nonexecutive|JJ "&
              "director|NN of|IN this|DT British|JJ industrial|JJ conglomerate|NN "&
              ".|.\nA|DT form|NN of|IN asbestos|NN once|RB used|VBN to|TO make|VB "&
              "Kent|NNP cigarette|NN filters|NNS has|VBZ caused|VBN a|DT high|JJ "&
              "percentage|NN of|IN cancer|NN deaths|NNS among|IN a|DT group|NN "&
              "of|IN workers|NNS exposed|VBN to|TO it|PRP more|RBR than|IN "&
              "30|CD years|NNS ago|IN ,|, researchers|NNS reported|VBD .|.")
#echo wsj

#let wsj1 = open("/Users/apple/nltk_data/corpora/treebank/tagged/wsj_0001.pos")
#let wsj2 = open("/Users/apple/nltk_data/corpora/treebank/tagged/wsj_0001.pos")

proc parsed(raw_sentences:seq[string]):seq[tuple[words:seq[string],tags:seq[string]]]=
  result = @[]
  for sentence in raw_sentences:
    var words: seq[string] = @[]
    var tags:  seq[string] = @[]
    for l in sentence.split():
      #let l= line.split('|",1)
      let i = l.find('|')
      assert i > -1
      words.add(l[l.low..i-1])
      tags.add(l[i+1..l.high])
      #echo words[words.high]," , ",tags[tags.high]
    var r: tuple[words:seq[string],tags:seq[string]]
    r.words = words
    r.tags = tags
    result.add(r)

proc get_all_tags(sentences:seq[tuple[words:seq[string],tags:seq[string]]]):seq[string] =
  result = @[]
  for sentence in sentences:
    for tag in sentence.tags:
      result.add(tag)
  result = deduplicate(result)

proc trained(raw_sentences:seq[string],n_iter:int) :PerceptronTagger[string]=
  #all_tags  = unique(all_tags)
  let sentences = parsed(raw_sentences)
  trained[string](sentences,get_all_tags(sentences),n_iter)

let wsj_train = open("wsj-train.txt").readAll().split("\n") # [0..500]
let wsj_test = open("wsj-test.txt").readAll().split("\n") # [0..1000]
#echo wsj1s
let tagger = trained(wsj_train,5)
tagger.test(parsed(wsj_test))