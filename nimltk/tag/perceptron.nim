import tables,hashes

type
  WeightKey[F,T] = tuple[feature:F,tag:T]
  Perceptron*[F,T] = tuple
    weights: TableRef[WeightKey[F,T],float]
    tags: seq[T]
  
  PerceptronTrainer*[F,T] = tuple
    perceptron: Perceptron[F,T]
    maxTimeStamp: float
    timeStamps: TableRef[WeightKey[F,T],float]
    totals: TableRef[WeightKey[F,T],float]

# weights:
# feature * tag
proc hash[F,T](x: WeightKey[F,T]): Hash {.inline.} =
  result = x.feature.hash !& x.tag.hash
  result = !$result


proc predict*[F,T](perceptron: Perceptron[F,T],features:Table[F,float]):T=
  var scores = newTable[T,float]()
  for feature, value in features.pairs():
    for tag in perceptron.tags:
      var weightKey: WeightKey[F,T]
      weightKey.feature = feature
      weightKey.tag = tag
      if not perceptron.weights.hasKey(weightKey):
        continue
      let w = perceptron.weights[weightKey]
      scores[tag] = scores[tag] + value*w
  result = perceptron.tags[0]
  var max_score = scores[result]
  for tag in perceptron.tags[1..perceptron.tags.high]:
    if max_score < scores[tag]:
      result = tag
      max_score = scores[tag]

proc updateInner[F,T](perceptron:var Perceptron[F,T],
            maxTimeStamp: var float,
            timeStamps:var TableRef[WeightKey[F,T],float],
            totals:var TableRef[WeightKey[F,T],float],
            weightKey:WeightKey[F,T],
            v:float) =
  let w = perceptron.weights[weightKey]
  let dt = (maxTimeStamp - timeStamps[weightKey])
  totals[weightKey] = totals[weightKey] + dt*w
  timeStamps[weightKey] = maxTimeStamp
  perceptron.weights[weightKey] = perceptron.weights[weightKey]+v

proc update[F,T](perceptron:var Perceptron[F,T],
                 maxTimeStamp: var float,
                 timeStamps:var TableRef[WeightKey[F,T],float],
                 totals:var TableRef[WeightKey[F,T],float],
                 features:Table[F,float],
                 guess:T,
                 truth:T)=
  if truth == guess:
    return
  maxTimeStamp += 1
  for feature, weight in features.pairs():
    updateInner(perceptron,maxTimeStamp,timeStamps,totals,(feature,truth),1.0*weight)
    updateInner(perceptron,maxTimeStamp,timeStamps,totals,(feature,guess),-1.0*weight)

proc update*[F,T](trainer:var PerceptronTrainer[F,T],features:Table[F,float],guess:T,truth:T) =
  update[F,T](trainer.perceptron, trainer.maxTimeStamp, trainer.timeStamps, trainer.totals, features,guess,truth)

proc averageWeights*[F,T](trainer:var PerceptronTrainer[F,T]) =
  for feature, weight in trainer.perceptron.weights.pairs():
    var total = trainer.totals[feature]
    total += (trainer.maxTimeStamp - trainer.timeStamps[feature]) * weight
    let average = total / trainer.maxTimeStamp
    #if average != 0:
    trainer.perceptron.weights[feature] = average

proc initTrainer*[F,T](tags:seq[T]):PerceptronTrainer[F,T] =
  result.perceptron.tags = tags
  result.perceptron.weights = newTable[WeightKey[F,T],float]()
  result.maxTimeStamp = 0.0
  result.timeStamps = newTable[WeightKey[F,T],float]()
  result.totals = newTable[WeightKey[F,T],float]()
