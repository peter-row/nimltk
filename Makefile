test:
	nim c -r tests/all 
	
test_fast:
	nim c -d:release -r tests/all

profile:
	nim c -r --profiler:on --threads:on --stackTrace:on tests/all 

profile_fast:
	nim c -d:release -r --profiler:on --stackTrace:on tests/all 

py:
	#nim c --compile_only --header:nimltk.h --app:lib nimltk
	#nim c --noMain --noLinking --app:lib nimltk
	#gcc -o m -Inimcache -I/Users/apple/repos/nim/lib nimcache/*.c test.c
	#clang -E -Inimcache -I/Users/apple/repos/Nim/lib nimcache/*.c > preprocessed.c
	#nim c -d:release --header:nimltk.h --app:lib nimltk
	nim c -d:debug --header:nimltk.h --app:lib nimltk
	#python build_ffi.py
	#python -m py_nimltk.corpus.reader.ffi_ex.py

py_fast:
	nim c -d:release --header:nimltk.h --app:lib nimltk
	
process: py
	clang -E -Inimcache -I/Users/apple/repos/Nim/lib nimcache/*.c > preprocessed.c
