if __name__ == "__main__" and __package__ is None:
    __package__ = "py_nimltk"

from PyNimFFI import nim_cdef, nim_wrap
from cffi import FFI

output_f = 'seqdefs.json'
ffi = FFI()
nim_cdef(ffi,output_f)
C = ffi.dlopen('libnimltk.dylib')
nim_wrap(ffi, C, output_f)
C.NimMain()
