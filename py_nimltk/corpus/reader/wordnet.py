from nltk.corpus.reader.wordnet import *

def _load_lemma_pos_offset_map(reader):
    from collections import defaultdict
    import msgpack
    from _nimltk.corpus.reader.wordnet import corpus_reader
    _reader = corpus_reader(reader._root)
    items = _reader.offset_map_items()
    _lemma_pos_offset_map = defaultdict(dict)
    pos_count  = defaultdict(int)
    for (lemma,pos,v) in msgpack.loads(items[:]):
        _lemma_pos_offset_map[lemma][pos] = v
        if pos == ADJ:
            _lemma_pos_offset_map[lemma][ADJ_SAT]=v
    reader._lemma_pos_offset_map = _lemma_pos_offset_map

WordNetCorpusReader._load_lemma_pos_offset_map = _load_lemma_pos_offset_map
if __name__ == '__main__':
    import nltk
    import time
    print('loading wordnet')
    t0 = time.time()
    wn = WordNetCorpusReader(nltk.data.find('corpora/wordnet'), None)
    print time.time()-t0
    print('done loading')
    S = wn.synset
    L = wn.lemma
    print('getting a synset for go')
    move_synset = S('go.v.21')
    print(move_synset.name(), move_synset.pos(), move_synset.lexname())
    print(move_synset.lemma_names())
    print(move_synset.definition())
    print(move_synset.examples())